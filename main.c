// programme principal
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>

#include "type.h"
#include "io.h"
#include "darboux.h"

int main(int argc, char **argv)
{
  mnt *m, *d;
  struct timeval tv_init, tv_begin, tv_end;
  struct timeval tv_beg_scatter, tv_end_gather;

  if(argc < 2)
  {
    fprintf(stderr, "Usage: %s <input filename> [<output filename>]\n", argv[0]);
    exit(1);
  }
  gettimeofday( &tv_init, NULL);

  // READ INPUT
  gettimeofday( &tv_beg_scatter, NULL);
  m = mnt_read(argv[1]);

  // COMPUTE
  gettimeofday( &tv_begin, NULL);
  d = darboux(m);
  gettimeofday( &tv_end, NULL);

  // WRITE OUTPUT
  FILE *out;
  if(argc == 3)
    out = fopen(argv[2], "w");
  else
    out = stdout;
  mnt_write(d, out);
  gettimeofday( &tv_end_gather, NULL);
  if(argc == 3)
    fclose(out);
  else
    mnt_write_lakes(m, d, stdout);

  // free
  free(m->terrain);
  free(m);
  free(d->terrain);
  free(d);

  fprintf(stderr, "Init : %lfs, Compute : %lf, End : %lf\n",
                                DIFFTEMPS(tv_init,tv_begin),
                                DIFFTEMPS(tv_begin,tv_end),
                                DIFFTEMPS(tv_beg_scatter, tv_begin) + DIFFTEMPS(tv_end, tv_end_gather));

  return(0);
}
